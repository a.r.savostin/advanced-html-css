const ourRecentPostsBlock = document.querySelector('.our_recent_posts_wrap');

const sizeTablet = 768;


window.addEventListener('resize', () => {
    cutPostMoreThree();
})

function cutPostMoreThree() {
    if (window.innerWidth <= sizeTablet && ourRecentPostsBlock.children.length > 3) {
        Array.from(ourRecentPostsBlock.children).forEach((el, index) => {
            if (index >= 3) {
                el.classList.add('hidden_post')
            }
        });
    }
    else {
        Array.from(ourRecentPostsBlock.children).forEach(el => el.classList.remove('hidden_post'))
    }
}
