const headerBurgerBtn = document.querySelector('.burger_nav_btn');
const burgerNavList = document.querySelector('.burger_nav_list');
const burgerNavListItem = document.querySelectorAll('.burger_nav_list_item');

headerBurgerBtn.addEventListener('click', (e) => {
    if (burgerNavList.classList.contains('hidden')) {
        headerBurgerBtn.classList.add('active');
        burgerNavList.classList.remove('hidden');
        burgerNavListItem.forEach(el => {
            el.classList.remove('hidden')
        });
        burgerNavList.style.height = 100 * burgerNavListItem.length + '%';
    } else {
        headerBurgerBtn.classList.remove('active');
        burgerNavList.classList.add('hidden');
        burgerNavListItem.forEach(el => {
            el.classList.add('hidden')
        });
        burgerNavList.removeAttribute('style')
    }

})