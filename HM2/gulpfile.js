const gulp = require('gulp'); // gulp
const { watch, series, parallel } = require('gulp'); // methods gulp
const autoPrefixer = require('gulp-autoprefixer'); //add prefix to css
const sass = require('gulp-sass')(require('sass')); // build scc
const browserSync = require('browser-sync').create(); // live server
const reload = browserSync.reload; //reload browser
const clean = require('gulp-clean'); // del folder
const cleanCSS = require('gulp-clean-css'); // clean css
const imagemin = require('gulp-imagemin'); // optimisate imgs
const minifyJs = require('gulp-js-minify');
const fileInclude = require('gulp-file-include');
var rename = require("gulp-rename");
var concat = require('gulp-concat');

function concatJs() {
    return gulp.src('./src/js/**/*.js')
        .pipe(concat('main.js'))
        .pipe(gulp.dest('./dist/js/'));
};

function includesHtml() {
    return gulp.src(['./src/*.html'])
        .pipe(fileInclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest('./dist'));
};


function jsBuild() {
    return gulp.src('./dist/js/main.js')
        .pipe(minifyJs())
        .pipe(rename('js/main.min.js'))
        .pipe(gulp.dest('./dist/'));
};

function imgsMin() {
    return gulp.src('src/imgs/**/*.{jpg,jpeg,png,svg,gif,ico,webp}')
        .pipe(imagemin())
        .pipe(gulp.dest('./dist/imgs/'));
}

// CLEAN DIST FOLDER
function clear() {
    return gulp.src('./dist', { read: false, allowEmpty: true }).
        pipe(clean());
}

// START LIVE SERVER BROWSER
function liveServer() {
    browserSync.init({
        server: {
            baseDir: './dist/',
            index: "index.html",
        },
        notify: false,
        port: 3000,
    });

    watch('./src/**/*.*', series(copyImgs, includesHtml, buildMinScc, concatJs, jsBuild)).on('change', reload);
};


// COPY HTML
function copyImgs() {
    return gulp.src('./src/imgs/*').pipe(gulp.dest('./dist/imgs'));
}


// BUILD SCC
function buildCSS() {
    return gulp.src('./src/scss/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoPrefixer({
            grid: true,
            overrideBrowserslist: ["last 3 versions"],
            cascade: true
        }))
        .pipe(gulp.dest('./dist/css'));
};

// BUILD SCC.MIN
function buildMinScc() {
    return gulp.src('./src/scss/main.scss').
        pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(autoPrefixer({
            grid: true,
            overrideBrowserslist: ["last 3 versions"],
            cascade: true
        }))
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(rename('css/main.min.css'))
        .pipe(gulp.dest('./dist/'))
}


exports.default = series(clear, copyImgs, includesHtml, buildMinScc, concatJs, jsBuild, liveServer);
exports.build = series(clear, includesHtml, buildCSS, buildMinScc, concatJs, jsBuild, imgsMin);




