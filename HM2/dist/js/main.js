const headerBurgerBtn = document.querySelector('.burger_nav_btn');
const burgerNavList = document.querySelector('.burger_nav_list');
const burgerNavListItem = document.querySelectorAll('.burger_nav_list_item');

headerBurgerBtn.addEventListener('click', (e) => {
    if (burgerNavList.classList.contains('hidden')) {
        headerBurgerBtn.classList.add('active');
        burgerNavList.classList.remove('hidden');
        burgerNavListItem.forEach(el => {
            el.classList.remove('hidden')
        });
        burgerNavList.style.height = 100 * burgerNavListItem.length + '%';
    } else {
        headerBurgerBtn.classList.remove('active');
        burgerNavList.classList.add('hidden');
        burgerNavListItem.forEach(el => {
            el.classList.add('hidden')
        });
        burgerNavList.removeAttribute('style')
    }

})
const ourRecentPostsBlock = document.querySelector('.our_recent_posts_wrap');

const sizeTablet = 768;


window.addEventListener('resize', () => {
    cutPostMoreThree();
})

function cutPostMoreThree() {
    if (window.innerWidth <= sizeTablet && ourRecentPostsBlock.children.length > 3) {
        Array.from(ourRecentPostsBlock.children).forEach((el, index) => {
            if (index >= 3) {
                el.classList.add('hidden_post')
            }
        });
    }
    else {
        Array.from(ourRecentPostsBlock.children).forEach(el => el.classList.remove('hidden_post'))
    }
}
